# Submission Queue

Before opening a ticket for submitting an app, please take your time to check if the app:

1. meets our
   [inclusion citeria](https://f-droid.org/wiki/page/Inclusion_Policy)

2. is already in the
   [fdroiddata repository](https://gitlab.com/search?scope=issues&project_id=36528)

3. has already [been requested](https://gitlab.com/search?scope=issues&project_id=2167965)

4. Consider packaging it yourself, then opening a merge request with
   the required metadata which will save you and us a lot of time, see
   the
   [contribution guideline](https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md)
   and
   [fdroiddata readme](https://gitlab.com/fdroid/fdroiddata/blob/master/README.md)

5. Then, if you still have not found the app you are interested in,
open a new [Request For Packaging](https://gitlab.com/fdroid/rfp/issues/new).


### Former Queues

These are listed here as an archive, they are no longer active:

* [Forum Search (old)](https://f-droid.org/forums/search/)
* [Submission Queue (old)](https://f-droid.org/forums/forum/submission-queue/)
* [Held Submissions (old)](https://f-droid.org/forums/forum/submissions-held/)
* [Completed Submissions (old)](https://f-droid.org/forums/forum/submissions-complete/)
